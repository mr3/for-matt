The text output library for Matt.
 
Pre-reqs: GCC c++ compiler that supports -std=C++20

The text output library for Matt is a header only library.  Cmake is required to run and build tests.
 
# Why

std::format is pretty good, but it returns a std::string whose data allocated on the heap.

    std::cout << std::format("I for {} don't think it's {} hard to avoid runtime processing\n", 1, 2);

The above does the following:

1. Initialises cout io stream state.
1. Parses the format string looking for formatting and configuring each location.
1. Constructs a string on the heap
1. Concattenating substrings to the string re-sizing if necessary.

I think Matt's way is better.

# Use
 
    #include "matt/matt.hpp"
    
    int main()
    {
        matt::print("I, for ", 1, ", don't think it's ", 2, " hard to avoid runtime processing", '\n'); 
    }

This is more efficient and keeps contextual information closer together.

If you want to print a hex or oct number do the following.

    matt::print(matt::hex(1), " ", matt:oct(1));

# Extend
    
## Output Device

    matt::print(args...);
    
Is a convenience wrapper for

    matt::printToDevice(stdOutDevice, args..);

A device class fullfils the Device concepts.  Included in this library for Matt, are some device examples.

- StdDevice for standard out and standard error.
- StringDevice for output to a string.
- LineBufferedDevice wraps another device and buffers output to it, flushing automatically when the buffer is full or on '\n'


## A type raster function.

An overloaded raster function are raster examples, including hex and oct generators.
   
 
    The text output library for Matt.
    Copyright (C) 2020 Roderick David Taylor
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.