//    The text output library for Matt.
//    Copyright (C) 2020 Roderick David Taylor
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MATT_STDDEVICE_HPP
#define MATT_STDDEVICE_HPP

#include <unistd.h>
#include <string_view>

namespace matt
{

template <int FILE_DES>
class StdDevice
{
public:
	StdDevice()
	{}

	void render(const char byte)
	{
		::write(FILE_DES, &byte, 1);
	}

	void render(std::string_view bytes)
	{
		::write(FILE_DES, bytes.data(), bytes.length());
	}

	void flush()
	{
		::fsync(FILE_DES);
	}
};

inline StdDevice<1> stdOutDevice;
inline StdDevice<2> stdErrDevice;

} // namespace matt


#endif /* MATT_STDDEVICE_HPP */
