//    The text output library for Matt.
//    Copyright (C) 2020 Roderick David Taylor
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MATT_RASTERARITMETIC_HPP
#define MATT_RASTERARITMETIC_HPP

#include <cstddef>
#include <limits>
#include <algorithm>
#include <bit>
#include <type_traits>

#include "Device.hpp"
#include "BufDevice.hpp"


namespace matt {

template <typename T>
concept SignedIntegral = std::is_signed_v<T> and std::is_integral_v<T>;

template <typename T>
concept UnsignedIntegral = std::is_unsigned_v<T> and std::is_integral_v<T>;

template <typename T>
concept Integral = std::is_integral_v<T>;

template<Integral IntegerType>
struct HexInt {
	IntegerType value;
};

template<Integral IntegerType>
constexpr auto hex(IntegerType i) ->  HexInt<IntegerType> {
	return HexInt<IntegerType> { i };
}

template<Integral IntegerType>
struct OctInt {
	IntegerType value;
};

template<Integral IntegerType>
constexpr auto oct(IntegerType i) -> OctInt<IntegerType> {
	return OctInt<IntegerType> { i };
}

inline char lookupAsciiHex(const std::size_t number) {
	static const char table[] { '0', '1', '2', '3', '4', '5', '6', '7', '8',
			'9', 'A', 'B', 'C', 'D', 'E', 'F' };
	return table[number];
}

template<Device DeviceType, Integral IntegerType>
inline void raster(DeviceType& dev, const HexInt<IntegerType>& arg) {
	dev.render("0x");

  using unsigned_type = typename std::make_unsigned<IntegerType>::type;

	constexpr int START_BITS = (sizeof(IntegerType) * 8) - 4;

  for(int i = START_BITS; i >= 0; i -= 4)
	{
    dev.render(lookupAsciiHex(static_cast<unsigned_type>(arg.value >> i) bitand 0xf));
  }
}

template<Device DeviceType, Integral IntegerType>
inline void raster(DeviceType& dev, const OctInt<IntegerType>& arg) {
	dev.render('0');

	using unsigned_type = typename std::make_unsigned<IntegerType>::type;

	constexpr int BITS = (sizeof(IntegerType) * 8);
	constexpr int START_BITS = BITS - (BITS % 3);

	for(int i = START_BITS; i >= 0; i -= 3)
	{
		dev.render(lookupAsciiHex((static_cast<unsigned_type>(arg.value) >> i) bitand 07));
	}
}

template <Device DeviceType, UnsignedIntegral IntegerType>
inline void raster(DeviceType& dev, const IntegerType& arg_in)
{
  // TODO: replace with something like in rasterFloat/rasterDouble that does digits at a time.
	IntegerType arg = arg_in;
	constexpr std::size_t MAX_DIGITS = std::numeric_limits<IntegerType>::digits10 + 1;
	char tmp[MAX_DIGITS];
	std::size_t i = 0;
	for(; i < MAX_DIGITS; ++i)
	{
		tmp[i] = lookupAsciiHex(static_cast<std::size_t>(arg) % 10);
		arg = arg / 10;
		if (0 == arg)
		{
			break;
		}
	}

	for(;;)
	{
		dev.render(tmp[i]);
		if(i == 0)
		{
			break;
		}
		--i;
	}
}

template<Device DeviceType, SignedIntegral IntegerType>
inline void raster(DeviceType& dev, const IntegerType& arg_in)
{
	constexpr std::size_t MAX_DIGITS = std::numeric_limits<IntegerType>::digits10 + 1;
	char tmp[MAX_DIGITS];

	/* This wierd negative hack is to accomodate SignedIntegral minimum values because
	 * Their magnitude is generally larger the maximum.
   * Make all numbers negative and then make positive before rendering.
	 */
	IntegerType arg = arg_in;
	if (arg > 0)
	{
		arg = static_cast<IntegerType>(-arg);
	}
	else
	{
		dev.render('-');
	}
	std::size_t i = 0;
	for(; i < MAX_DIGITS; ++i)
	{
		tmp[i] = lookupAsciiHex(static_cast<std::size_t>(-(arg % 10)));
		arg = arg / 10;
		if (0 == arg)
		{
			break;
		}
	}

	for(;;)
	{
		dev.render(tmp[i]);
		if(i == 0)
		{
			break;
		}
		--i;
	}
}

} // namespace matt

#endif /* MATT_RASTERARITMETIC_HPP */

