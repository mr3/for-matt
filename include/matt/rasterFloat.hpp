#ifndef MATT_RASTER_FLOAT_HPP
#define MATT_RASTER_FLOAT_HPP

#include <type_traits>
#include "dragonbox/dragonbox.h"

namespace matt
{

  struct Float
  {
    float val;
  };

  constexpr Float operator ""_f(long double x) { return Float{static_cast<float>(x)};}

  namespace dragonbox_impl
  {
    // Everything in this namespace is Adapted from jkj's dragonbox_to_chars function found in third-party/dragonbox.
    // Essentially replacing buffer manipulation with rending to the devices.

    using namespace jkj::dragonbox;

    // These "//"'s are to prevent clang-format to ruin this nice alignment.
    // Thanks to reddit user u/mcmcc:
    // https://www.reddit.com/r/cpp/comments/so3wx9/dragonbox_110_is_released_a_fast_floattostring/hw8z26r/?context=3
    static constexpr char radix_100_table[] = {
        '0', '0', '0', '1', '0', '2', '0', '3', '0', '4', //
        '0', '5', '0', '6', '0', '7', '0', '8', '0', '9', //
        '1', '0', '1', '1', '1', '2', '1', '3', '1', '4', //
        '1', '5', '1', '6', '1', '7', '1', '8', '1', '9', //
        '2', '0', '2', '1', '2', '2', '2', '3', '2', '4', //
        '2', '5', '2', '6', '2', '7', '2', '8', '2', '9', //
        '3', '0', '3', '1', '3', '2', '3', '3', '3', '4', //
        '3', '5', '3', '6', '3', '7', '3', '8', '3', '9', //
        '4', '0', '4', '1', '4', '2', '4', '3', '4', '4', //
        '4', '5', '4', '6', '4', '7', '4', '8', '4', '9', //
        '5', '0', '5', '1', '5', '2', '5', '3', '5', '4', //
        '5', '5', '5', '6', '5', '7', '5', '8', '5', '9', //
        '6', '0', '6', '1', '6', '2', '6', '3', '6', '4', //
        '6', '5', '6', '6', '6', '7', '6', '8', '6', '9', //
        '7', '0', '7', '1', '7', '2', '7', '3', '7', '4', //
        '7', '5', '7', '6', '7', '7', '7', '8', '7', '9', //
        '8', '0', '8', '1', '8', '2', '8', '3', '8', '4', //
        '8', '5', '8', '6', '8', '7', '8', '8', '8', '9', //
        '9', '0', '9', '1', '9', '2', '9', '3', '9', '4', //
        '9', '5', '9', '6', '9', '7', '9', '8', '9', '9'  //
    };
    static constexpr char radix_100_head_table[] = {
        '0', '.', '1', '.', '2', '.', '3', '.', '4', '.', //
        '5', '.', '6', '.', '7', '.', '8', '.', '9', '.', //
        '1', '.', '1', '.', '1', '.', '1', '.', '1', '.', //
        '1', '.', '1', '.', '1', '.', '1', '.', '1', '.', //
        '2', '.', '2', '.', '2', '.', '2', '.', '2', '.', //
        '2', '.', '2', '.', '2', '.', '2', '.', '2', '.', //
        '3', '.', '3', '.', '3', '.', '3', '.', '3', '.', //
        '3', '.', '3', '.', '3', '.', '3', '.', '3', '.', //
        '4', '.', '4', '.', '4', '.', '4', '.', '4', '.', //
        '4', '.', '4', '.', '4', '.', '4', '.', '4', '.', //
        '5', '.', '5', '.', '5', '.', '5', '.', '5', '.', //
        '5', '.', '5', '.', '5', '.', '5', '.', '5', '.', //
        '6', '.', '6', '.', '6', '.', '6', '.', '6', '.', //
        '6', '.', '6', '.', '6', '.', '6', '.', '6', '.', //
        '7', '.', '7', '.', '7', '.', '7', '.', '7', '.', //
        '7', '.', '7', '.', '7', '.', '7', '.', '7', '.', //
        '8', '.', '8', '.', '8', '.', '8', '.', '8', '.', //
        '8', '.', '8', '.', '8', '.', '8', '.', '8', '.', //
        '9', '.', '9', '.', '9', '.', '9', '.', '9', '.', //
        '9', '.', '9', '.', '9', '.', '9', '.', '9', '.'  //
    };

    template <Device DeviceType>
    inline static void print_1_digit(std::uint32_t n, DeviceType& device)
    {
      if constexpr ('1' == '0' + 1 && '2' == '0' + 2 && '3' == '0' + 3 && '4' == '0' + 4 &&
                    '5' == '0' + 5 && '6' == '0' + 6 && '7' == '0' + 7 && '8' == '0' + 8 &&
                    '9' == '0' + 9)
      {
        // RDT: this is interesting... I thought the above would always evaluate to true in c++.
        // It's compile time anyway... so leaving it here.
        if constexpr (('0' & 0xf) == 0)
        {
          device.render(char('0' | n));
        }
        else
        {
          device.render(char('0' + n));
        }
      }
      else
      {
        device.render(radix_100_table[n * 2 + 1]);
      }
    }

    template <Device DeviceType>
    inline static void print_2_digits(std::uint32_t n, DeviceType& device)
    {
      device.render(std::string_view(&radix_100_table[n * 2], 2));
    }

    template <Device DeviceType>
    inline static void Print2NoZeros(std::uint32_t n, DeviceType& device)
    {
      if (*(radix_100_table + n * 2 + 1) > '0')
      {
        // 0  not detected as the final digit, so print both.
        print_2_digits(n, device);
      }
      else
      {
        // final digit is 0, print only the first.
        device.render(radix_100_table[n * 2]);
      }
    }

    template <Device DeviceType>
    inline static void PrintHeadOnly(std::uint32_t n, DeviceType& device)
    {
      if ((n >= 10) && (radix_100_table[n * 2 + 1] > '0')) // more than 2 digits, and second not zero.
      {
        // Write the first digit and the decimal point.
        device.render(std::string_view(&radix_100_head_table[n * 2], 2));

        // and the non-zero second digit
        device.render(radix_100_table[n * 2 + 1]);
      }
      else // false, a single digit
      {
        // Write the first digit without the decimal point.
        device.render(radix_100_head_table[n * 2]);
      }
    }

    template <Device DeviceType>
    inline static void PrintHead(std::uint32_t n, DeviceType& device)
    {
      if ((n >= 10) && (radix_100_table[n * 2 + 1] > '0')) // more than 2 digits, and second not zero.
      {
        // Write the first digit and the decimal point.
        device.render(std::string_view(&radix_100_head_table[n * 2], 2));

        // then the 2nd digit
        device.render(radix_100_table[n * 2 + 1]);
      }
      else // false, a single digit
      {
        // Write the first digit and the decimal point.
        device.render(std::string_view(&radix_100_head_table[n * 2], 2));
      }
    }

    // These digit generation routines are inspired by James Anhalt's itoa algorithm:
    // https://github.com/jeaiii/itoa
    // The main idea is for given n, find y such that floor(10^k * y / 2^32) = n holds,
    // where k is an appropriate integer depending on the length of n.
    // For example, if n = 1234567, we set k = 6. In this case, we have
    // floor(y / 2^32) = 1,
    // floor(10^2 * ((10^0 * y) mod 2^32) / 2^32) = 23,
    // floor(10^2 * ((10^2 * y) mod 2^32) / 2^32) = 45, and
    // floor(10^2 * ((10^4 * y) mod 2^32) / 2^32) = 67.
    // See https://jk-jeon.github.io/posts/2022/02/jeaiii-algorithm/ for more explanation.

    template <Device DeviceType>
    inline static void print_9_digits(std::uint32_t s32, int& exponent,
                                      DeviceType &device)
    {
      // -- IEEE-754 binary32
      // Since we do not cut trailing zeros in advance, s32 must be of 6~9 digits
      // unless the original input was subnormal.
      // In particular, when it is of 9 digits it shouldn't have any trailing zeros.
      // -- IEEE-754 binary64
      // In this case, s32 must be of 7~9 digits unless the input is subnormal,
      // and it shouldn't have any trailing zeros if it is of 9 digits.
      if (s32 >= 1'0000'0000)
      {
        // 9 digits.
        // 1441151882 = ceil(2^57 / 1'0000'0000) + 1
        auto prod = s32 * std::uint64_t(1441151882);
        prod >>= 25;
        device.render(std::string_view(&radix_100_head_table[std::uint32_t(prod >> 32) * 2], 2));

        prod = std::uint32_t(prod) * std::uint64_t(100);
        print_2_digits(std::uint32_t(prod >> 32), device);
        prod = std::uint32_t(prod) * std::uint64_t(100);
        print_2_digits(std::uint32_t(prod >> 32), device);
        prod = std::uint32_t(prod) * std::uint64_t(100);
        print_2_digits(std::uint32_t(prod >> 32), device);
        prod = std::uint32_t(prod) * std::uint64_t(100);
        print_2_digits(std::uint32_t(prod >> 32), device);

        exponent += 8;
      }
      else if (s32 >= 100'0000)
      {
        // 7 or 8 digits.
        // 281474978 = ceil(2^48 / 100'0000) + 1
        auto prod = s32 * std::uint64_t(281474978);
        prod >>= 16;
        auto prod_hi = std::uint32_t(prod >> 32);
        // If s32 is of 8 digits, increase the exponent by 7.
        // Otherwise, increase it by 6.
        exponent += (6 + static_cast<int>(prod_hi >= 10));

        if (std::uint32_t(prod) <= std::uint32_t((std::uint64_t(1) << 32) / 100'0000))
        {
          // The last 6 digits are all zeros
          PrintHeadOnly(prod_hi, device);
        }
        else
        {
          PrintHead(prod_hi, device);

          // At least one of the last 6 digits are nonzero.

          prod = std::uint32_t(prod) * std::uint64_t(100);
          prod_hi = std::uint32_t(prod >> 32);

          if (std::uint32_t(prod) <= std::uint32_t((std::uint64_t(1) << 32) / 1'0000))
          {
            // The last 4 digits are all zero
            Print2NoZeros(prod_hi, device);
          }
          else
          {
            // print 3rd and 4th digit
            print_2_digits(prod_hi, device);

            // At least one of the remaining 4 digits are nonzero.

            // Obtain the next two digits.
            prod = std::uint32_t(prod) * std::uint64_t(100);
            prod_hi = std::uint32_t(prod >> 32);

            if (std::uint32_t(prod) <= std::uint32_t((std::uint64_t(1) << 32) / 100))
            {
              Print2NoZeros(prod_hi, device);
            }
            else
            {
              print_2_digits(prod_hi, device);
              prod = std::uint32_t(prod) * std::uint64_t(100);

              prod_hi = std::uint32_t(prod >> 32);
              Print2NoZeros(prod_hi, device);
            }
          }
        }
      }
      else if (s32 >= 1'0000)
      {
        // 5 or 6 digits.
        // 429497 = ceil(2^32 / 1'0000)
        auto prod = s32 * std::uint64_t(429497);
        auto prod_hi = std::uint32_t(prod >> 32);

        // If s32 is of 6 digits, increase the exponent by 5.
        // Otherwise, increase it by 4.
        exponent += (4 + static_cast<int>(prod_hi >= 10));

        if (std::uint32_t(prod) <= std::uint32_t((std::uint64_t(1) << 32) / 1'0000))
        {
          PrintHeadOnly(prod_hi, device);
        }
        else
        {
          // print 3rd and 4th digit
          print_2_digits(prod_hi, device);

          // At least one of the remaining 4 digits are nonzero.

          // Obtain the next two digits.
          prod = std::uint32_t(prod) * std::uint64_t(100);
          prod_hi = std::uint32_t(prod >> 32);

          if (std::uint32_t(prod) <= std::uint32_t((std::uint64_t(1) << 32) / 100))
          {
            Print2NoZeros(prod_hi, device);
          }
          else
          {
            print_2_digits(prod_hi, device);
            prod = std::uint32_t(prod) * std::uint64_t(100);

            prod_hi = std::uint32_t(prod >> 32);
            Print2NoZeros(prod_hi, device);
          }
        }
      }
      else if (s32 >= 100)
      {
        // 3 or 4 digits.
        // 42949673 = ceil(2^32 / 100)
        auto prod = s32 * std::uint64_t(42949673);
        auto prod_hi = std::uint32_t(prod >> 32);

        // If s32 is of 4 digits, increase the exponent by 3.
        // Otherwise, increase it by 2.
        exponent += (2 + int(prod_hi >= 10));

        // Remaining 2 digits are all zero?
        if (std::uint32_t(prod) <= std::uint32_t((std::uint64_t(1) << 32) / 100))
        {
          PrintHeadOnly(prod_hi, device);
        }
        else
        {
          print_2_digits(prod_hi, device);
          prod = std::uint32_t(prod) * std::uint64_t(100);

          prod_hi = std::uint32_t(prod >> 32);
          Print2NoZeros(prod_hi, device);
        }
      }
      else
      {
        PrintHeadOnly(s32, device);
      }
    }

    template <Device DeviceType>
    void to_chars_float(std::uint32_t s32, int exponent, DeviceType &device)
    {
      // Print significand.
      print_9_digits(s32, exponent, device);

      // Print exponent and return
      if (exponent < 0)
      {
        device.render("E-");
        exponent = -exponent;
      }
      else
      {
        device.render('E');
      }

      if (exponent >= 10)
      {
        print_2_digits(std::uint32_t(exponent), device);
      }
      else
      {
        print_1_digit(std::uint32_t(exponent), device);
      }
    }


        template <Device DeviceType>
        void to_chars_double(std::uint64_t const significand, int exponent, DeviceType& device) {
            // Print significand by decomposing it into a 9-digit block and a 8-digit block.
            std::uint32_t first_block, second_block;
            bool no_second_block;

            if (significand >= 1'0000'0000) {
                first_block = std::uint32_t(significand / 1'0000'0000);
                second_block = std::uint32_t(significand) - first_block * 1'0000'0000;
                exponent += 8;
                no_second_block = (second_block == 0);
            }
            else {
                first_block = std::uint32_t(significand);
                no_second_block = true;
            }

            if (no_second_block) {
                print_9_digits(first_block, exponent, device);
            }
            else {
                // We proceed similarly to print_9_digits(), but since we do not need to remove
                // trailing zeros, the procedure is a bit simpler.
                if (first_block >= 1'0000'0000) {
                    // The input is of 17 digits, thus there should be no trailing zero at all.
                    // The first block is of 9 digits.
                    // 1441151882 = ceil(2^57 / 1'0000'0000) + 1
                    auto prod = first_block * std::uint64_t(1441151882);
                    prod >>= 25;
                    device.render(std::string_view(&radix_100_head_table[std::uint32_t(prod >> 32) * 2] , 2));
                    prod = std::uint32_t(prod) * std::uint64_t(100);
                    print_2_digits(std::uint32_t(prod >> 32), device);
                    prod = std::uint32_t(prod) * std::uint64_t(100);
                    print_2_digits(std::uint32_t(prod >> 32), device);
                    prod = std::uint32_t(prod) * std::uint64_t(100);
                    print_2_digits(std::uint32_t(prod >> 32), device);
                    prod = std::uint32_t(prod) * std::uint64_t(100);
                    print_2_digits(std::uint32_t(prod >> 32), device);

                    // The second block is of 8 digits.
                    // 281474978 = ceil(2^48 / 100'0000) + 1
                    prod = second_block * std::uint64_t(281474978);
                    prod >>= 16;
                    prod += 1;
                    print_2_digits(std::uint32_t(prod >> 32), device);
                    prod = std::uint32_t(prod) * std::uint64_t(100);
                    print_2_digits(std::uint32_t(prod >> 32), device);
                    prod = std::uint32_t(prod) * std::uint64_t(100);
                    print_2_digits(std::uint32_t(prod >> 32), device);
                    prod = std::uint32_t(prod) * std::uint64_t(100);
                    print_2_digits(std::uint32_t(prod >> 32), device);

                    exponent += 8;
                }
                else {
                    if (first_block >= 100'0000) {
                        // 7 or 8 digits.
                        // 281474978 = ceil(2^48 / 100'0000) + 1
                        auto prod = first_block * std::uint64_t(281474978);
                        prod >>= 16;
                        const auto prod_hi = std::uint32_t(prod >> 32);

                        PrintHead(prod_hi, device);
                        // Print remaining 6 digits.
                        prod = std::uint32_t(prod) * std::uint64_t(100);
                        print_2_digits(std::uint32_t(prod >> 32), device);
                        prod = std::uint32_t(prod) * std::uint64_t(100);
                        print_2_digits(std::uint32_t(prod >> 32), device);
                        prod = std::uint32_t(prod) * std::uint64_t(100);
                        print_2_digits(std::uint32_t(prod >> 32), device);

                        exponent += (6 + static_cast<int>(prod_hi >= 10));
                    }
                    else if (first_block >= 1'0000) {
                        // 5 or 6 digits.
                        // 429497 = ceil(2^32 / 1'0000)
                        auto prod = first_block * std::uint64_t(429497);
                        const auto prod_hi = std::uint32_t(prod >> 32);

                        PrintHead(prod_hi, device);
                        // Print remaining 4 digits.
                        prod = std::uint32_t(prod) * std::uint64_t(100);
                        print_2_digits(std::uint32_t(prod >> 32), device);
                        prod = std::uint32_t(prod) * std::uint64_t(100);
                        print_2_digits(std::uint32_t(prod >> 32), device);

                        exponent += (4 + static_cast<int>(prod_hi >= 10));

                    }
                    else if (first_block >= 100) {
                        // 3 or 4 digits.
                        // 42949673 = ceil(2^32 / 100)
                        auto prod = first_block * std::uint64_t(42949673);
                        auto const prod_hi = std::uint32_t(prod >> 32);

                        PrintHead(prod_hi, device);

                        // Print remaining 2 digits.
                        prod = std::uint32_t(prod) * std::uint64_t(100);
                        print_2_digits(prod_hi, device);

                        exponent += (2 + static_cast<int>(prod_hi >= 10));
                    }
                    else {
                        // 1 or 2 digits.
                        PrintHead(first_block, device);

                        exponent += static_cast<int>(first_block >= 10);
                    }

                    // Next, print the second block.
                    // The second block is of 8 digits, but we may have trailing zeros.
                    // 281474978 = ceil(2^48 / 100'0000) + 1
                    auto prod = second_block * std::uint64_t(281474978);
                    prod >>= 16;
                    prod += 1;
                    auto prod_hi = std::uint32_t(prod >> 32);

                    // Remaining 6 digits are all zero?
                    if (std::uint32_t(prod) <= std::uint32_t((std::uint64_t(1) << 32) / 100'0000)) {
                        Print2NoZeros(std::uint32_t(prod >> 32), device);
                    }
                    else {
                      print_2_digits(prod_hi, device);
                        // Obtain the next two digits.
                        prod = std::uint32_t(prod) * std::uint64_t(100);
                        prod_hi = std::uint32_t(prod >> 32);
                        // Remaining 4 digits are all zero?
                        if (std::uint32_t(prod) <=
                            std::uint32_t((std::uint64_t(1) << 32) / 1'0000)) {
                            Print2NoZeros(prod_hi, device);
                        }
                        else {
                            print_2_digits(prod_hi, device);
                            // Obtain the next two digits.
                            prod = std::uint32_t(prod) * std::uint64_t(100);
                            prod_hi = std::uint32_t(prod >> 32);

                            // Remaining 2 digits are all zero?
                            if (std::uint32_t(prod) <=
                                std::uint32_t((std::uint64_t(1) << 32) / 100)) {
                                Print2NoZeros(prod_hi, device);
                            }
                            else {
                                print_2_digits(prod_hi, device);
                                // Obtain the last two digits.
                                prod = std::uint32_t(prod) * std::uint64_t(100);
                                print_2_digits(prod_hi, device);
                            }
                        }
                    }
                }
            }

            // Print exponent and return
            if (exponent < 0) {
                device.render("E-");
                exponent = -exponent;
            }
            else {
                device.render('E');
            }

            if (exponent >= 100) {
                // d1 = exponent / 10; d2 = exponent % 10;
                // 6554 = ceil(2^16 / 10)
                auto prod = std::uint32_t(exponent) * std::uint32_t(6554);
                auto d1 = prod >> 16;
                prod = std::uint16_t(prod) * std::uint32_t(5); // * 10
                auto d2 = prod >> 15;                          // >> 16
                print_2_digits(d1, device);
                print_1_digit(d2, device);
            }
            else if (exponent >= 10) {
                print_2_digits(std::uint32_t(exponent), device);
            }
            else {
                print_1_digit(std::uint32_t(exponent), device);
            }
        }

    template <class PolicyHolder, Device DeviceType, typename Float, class FloatTraits>
    void raster(DeviceType &device, float_bits<Float, FloatTraits> br) noexcept
    {
      auto const exponent_bits = br.extract_exponent_bits();
      auto const s = br.remove_exponent_bits(exponent_bits);

      if (br.is_finite(exponent_bits))
      {
        if (s.is_negative())
        {
          device.render('-');
        }
        if (br.is_nonzero())
        {
          auto result = to_decimal<Float, FloatTraits>(
              s, exponent_bits, policy::sign::ignore, policy::trailing_zero::ignore,
              typename PolicyHolder::decimal_to_binary_rounding_policy{},
              typename PolicyHolder::binary_to_decimal_rounding_policy{},
              typename PolicyHolder::cache_policy{});
          if constexpr (std::is_same_v<float, Float>)
          {
            to_chars_float(result.significand, result.exponent, device);
          }
          else
          {
            to_chars_double(result.significand, result.exponent, device);
          }
        }
        else
        {
          device.render("0E0");
        }
      }
      else
      {
        if (s.has_all_zero_significand_bits())
        {
          if (s.is_negative())
          {
            device.render('-');
          }
          device.render("Infinity");
        }
        else
        {
          device.render("NaN");
        }
      }
    }
  } // namespace dragonbox_impl


  template <Device DeviceType>
  inline void raster(DeviceType &device, Float x)
  {
    using namespace jkj::dragonbox;
    using namespace jkj::dragonbox::detail::policy_impl;
    using policy_holder = decltype(make_policy_holder(
        base_default_pair_list<base_default_pair<decimal_to_binary_rounding::base,
                                                 decimal_to_binary_rounding::nearest_to_even>,
                               base_default_pair<binary_to_decimal_rounding::base,
                                                 binary_to_decimal_rounding::to_even>,
                               base_default_pair<cache::base, cache::full>>{}));

    dragonbox_impl::raster<policy_holder>(device, float_bits<float, default_float_traits<float>>(x.val));
  }


  template <Device DeviceType>
  inline void raster(DeviceType &device, double x)
  {
    using namespace jkj::dragonbox;
    using namespace jkj::dragonbox::detail::policy_impl;
    using policy_holder = decltype(make_policy_holder(
        base_default_pair_list<base_default_pair<decimal_to_binary_rounding::base,
                                                 decimal_to_binary_rounding::nearest_to_even>,
                               base_default_pair<binary_to_decimal_rounding::base,
                                                 binary_to_decimal_rounding::to_even>,
                               base_default_pair<cache::base, cache::full>>{}));

    dragonbox_impl::raster<policy_holder>(device, float_bits<double, default_float_traits<double>>(x));
  }

} // namespace matt

#endif /* MATT_RASTER_FLOAT_HPP */