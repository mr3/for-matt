//    The text output library for Matt.
//    Copyright (C) 2020 Roderick David Taylor
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MATT_LINEBUFFEREDDEVICE_HPP
#define MATT_LINEBUFFEREDDEVICE_HPP

#include <string_view>
#include <cstddef>

#include "BufferedDevice.hpp"
#include "Device.hpp"

namespace matt
{
	template <Device SubDeviceType, std::size_t BUFFER_SIZE = 1024>
	class LineBufferedDevice
	{
	public:

		template <typename ...Args>
		LineBufferedDevice(Args...args) : bufferedDevice(args...)
		{
		}

		void render(const char byte)
		{
			bufferedDevice.render(byte);
			if (byte == '\n')
			{
				this->flush();
			}
		}

		void render(std::string_view bytes)
		{
			for( auto byte : bytes)
			{
				this->render(byte);
			}
		}

		void flush()
		{
			bufferedDevice.flush();
		}

		auto getDevice() const ->  const SubDeviceType&
		{
			return bufferedDevice.getDevice();
		}

	private:
		BufferedDevice<SubDeviceType, BUFFER_SIZE> bufferedDevice;
	};
}



#endif /* MATT_LINEBUFFEREDDEVICE_HPP */
