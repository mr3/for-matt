//    The text output library for Matt.
//    Copyright (C) 2020 Roderick David Taylor
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MATT_MATT_HPP
#define MATT_MATT_HPP

#include "printToDevice.hpp"
#include "StdDevice.hpp"
#include "StringDevice.hpp"
#include "BufferedDevice.hpp"
#include "LineBufferedDevice.hpp"
#include "UnixFileDescriptorDevice.hpp"

namespace matt
{

template <typename...Args>
void print(Args...args)
{
	printToDevice(stdOutDevice, args...);
}

template <typename...Args>
void printLine(Args...args)
{
	printToDevice(stdOutDevice, args..., '\n');
}


} // namespace matt

#endif /* MATT_MATT_HPP */
