//    The text output library for Matt.
//    Copyright (C) 2020 Roderick David Taylor
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MATT_UNIXFILEDESCRIPTORDEVICE_HPP
#define MATT_UNIXFILEDESCRIPTORDEVICE_HPP

#include <unistd.h>
#include <string_view>

namespace matt
{

class UnixFileDescriptorDevice
{
public:
	UnixFileDescriptorDevice(const int fileDescriptor) : fileDescriptor_(fileDescriptor)
	{}

	void render(const char byte)
	{
		::write(fileDescriptor_, &byte, 1);
	}

	void render(std::string_view bytes)
	{
		::write(fileDescriptor_, bytes.data(), bytes.length());
	}

	void flush()
	{
		::fsync(fileDescriptor_);
	}

private:
	const int fileDescriptor_;
};

}



#endif /* MATT_UNIXFILEDESCRIPTORDEVICE_HPP */
