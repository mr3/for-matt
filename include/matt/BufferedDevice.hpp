//    The text output library for Matt.
//    Copyright (C) 2020 Roderick David Taylor
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MATT_BUFFEREDDEVICE_HPP
#define MATT_BUFFEREDDEVICE_HPP

#include <string_view>
#include <cstddef>

#include "Device.hpp"

namespace matt
{

template <Device SubDeviceType, std::size_t BUFFER_SIZE = 1024>
class BufferedDevice
{
public:

	template <typename ...Args>
	BufferedDevice(Args...args) : dev(args...)
	{}

	void render(const char byte)
	{
		buffer[next] = byte;
		++next;
		if (BUFFER_SIZE == next)
		{
			next = 0;
			dev.render(buffer);
		}
	}

	void render(std::string_view bytes)
	{
		for( auto byte : bytes)
		{
			this->render(byte);
		}
	}

	void flush()
	{
		dev.render(std::string_view(buffer, next));
		next = 0;
	}

	auto getDevice() const -> const SubDeviceType&
	{
		return dev;
	}


private:
	char buffer[BUFFER_SIZE] {};
	unsigned int next = 0;
	SubDeviceType dev;

};

} // namespace matt



#endif /* MATT_BUFFEREDDEVICE_HPP */
