//    The text output library for Matt.
//    Copyright (C) 2020 Roderick David Taylor
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MATT_RASTER_HPP
#define MATT_RASTER_HPP

#include <cstdlib>
#include <string_view>

#include "Device.hpp"
#include "rasterIntegral.hpp"
#include "rasterFloat.hpp"

namespace matt
{

template <Device DeviceType>
inline void raster(DeviceType& dev, const char& character)
{
	dev.render(character);
}

template <Device DeviceType>
inline void raster(DeviceType& dev, std::string_view characters)
{
	dev.render(characters);
}
} //namespace matt



#endif /* MATT_RASTER_HPP */
