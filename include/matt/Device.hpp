//    The text output library for Matt.
//    Copyright (C) 2020 Roderick David Taylor
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MATT_DEVICE_HPP
#define MATT_DEVICE_HPP

#include <string_view>

namespace matt
{

template <typename T>
concept Device = requires (T a, std::string_view buffer, char c) {
	{a.flush()};
	{a.render(c)};
	{a.render(buffer)};
};

} // namespace matt



#endif /* MATT_DEVICE_HPP */
