/*
 * tests.cpp
 *
 *  Created on: 22 Mar 2020
 *      Author: rdt
 */

#include <cstdint>
#include <string_view>
#include <limits>
#include <matt/matt.hpp>
#include "boost/ut.hpp"

//template <>
//auto boost::ut::cfg<boost::ut::override> = boost::ut::runner<boost::ut::junit::reporter>{};

template <matt::Integral IntegerType>
struct IntegerResultStrings
{
};

template <>
struct IntegerResultStrings<uint8_t>
{
  const std::string_view min{"0"};
  const std::string_view max{"255"};
  const std::string_view hex_max{"0xFF"};
  const std::string_view hex_min{"0x00"};
  const std::string_view oct_max{"0377"};
  const std::string_view oct_min{"0000"};
};

template <>
struct IntegerResultStrings<int8_t>
{
  const std::string_view min{"-128"};
  const std::string_view max{"127"};
  const std::string_view hex_max{"0x7F"};
  const std::string_view hex_min{"0x80"};
  const std::string_view oct_max{"0177"};
  const std::string_view oct_min{"0200"};
};

template <>
struct IntegerResultStrings<uint16_t>
{
  const std::string min{"0"};
  const std::string max{"65535"};
  const std::string_view hex_max{"0xFFFF"};
  const std::string_view hex_min{"0x0000"};
  const std::string_view oct_max{"0177777"};
  const std::string_view oct_min{"0000000"};
};

template <>
struct IntegerResultStrings<int16_t>
{
  const std::string_view min{"-32768"};
  const std::string_view max{"32767"};
  const std::string_view hex_max{"0x7FFF"};
  const std::string_view hex_min{"0x8000"};
  const std::string_view oct_max{"0077777"};
  const std::string_view oct_min{"0100000"};
};

template <>
struct IntegerResultStrings<uint32_t>
{
  const std::string_view min{"0"};
  const std::string_view max{"4294967295"};
  const std::string_view hex_max{"0xFFFFFFFF"};
  const std::string_view hex_min{"0x00000000"};
  const std::string_view oct_max{"037777777777"};
  const std::string_view oct_min{"000000000000"};
};

template <>
struct IntegerResultStrings<int32_t>
{
  const std::string_view min{"-2147483648"};
  const std::string_view max{"2147483647"};
  const std::string_view hex_max{"0x7FFFFFFF"};
  const std::string_view hex_min{"0x80000000"};
  const std::string_view oct_max{"017777777777"};
  const std::string_view oct_min{"020000000000"};
};

template <>
struct IntegerResultStrings<uint64_t>
{
  const std::string_view min{"0"};
  const std::string_view max{"18446744073709551615"};
  const std::string_view hex_max{"0xFFFFFFFFFFFFFFFF"};
  const std::string_view hex_min{"0x0000000000000000"};
  const std::string_view oct_max{"01777777777777777777777"};
  const std::string_view oct_min{"00000000000000000000000"};
};

template <>
struct IntegerResultStrings<int64_t>
{
  const std::string_view min{"-9223372036854775808"};
  const std::string_view max{"9223372036854775807"};
  const std::string_view hex_max{"0x7FFFFFFFFFFFFFFF"};
  const std::string_view hex_min{"0x8000000000000000"};
  const std::string_view oct_max{"00777777777777777777777"};
  const std::string_view oct_min{"01000000000000000000000"};
};

template <matt::Integral IntegerType>
std::string toString(IntegerType i)
{
  matt::StringDevice d;

  matt::printToDevice(d, i);

  return d.getString();
}

template <matt::Integral IntegerType>
std::string toHexString(IntegerType i)
{
  matt::StringDevice d;

  matt::printToDevice(d, matt::hex(i));

  return d.getString();
}

template <matt::Integral IntegerType>
std::string toOctString(IntegerType i)
{
  matt::StringDevice d;

  matt::printToDevice(d, matt::oct(i));

  return d.getString();
}

// void test_main()
int main()
{
  using namespace boost::ut;

  "integrals"_test = []<typename TestType>
  {
    expect(toString(std::numeric_limits<TestType>::min()) == IntegerResultStrings<TestType>().min) << "min ";
    expect(toString(std::numeric_limits<TestType>::max()) == IntegerResultStrings<TestType>().max) << "max ";
    expect(toHexString(std::numeric_limits<TestType>::min()) == IntegerResultStrings<TestType>().hex_min) << "hex min ";
    expect(toHexString(std::numeric_limits<TestType>::max()) == IntegerResultStrings<TestType>().hex_max) << "hex max ";
    expect(toOctString(std::numeric_limits<TestType>::min()) == IntegerResultStrings<TestType>().oct_min) << "oct min ";
    expect(toOctString(std::numeric_limits<TestType>::max()) == IntegerResultStrings<TestType>().oct_max) << "oct max ";
  } | std::tuple<uint8_t, int8_t, uint16_t, int16_t, uint32_t, int32_t, uint64_t, int64_t>{};

  "strings"_test = []
  {
    std::string test_string{"Hello World"};
    matt::StringDevice d;
    matt::printToDevice(d, "Hello World");
    expect(d.getString() == test_string) << "string ";
  };

  "linebuffer"_test = []
  {
    matt::LineBufferedDevice<matt::StringDevice> d;
    expect(d.getDevice().getString() == "") << "line buffer empty";
    printToDevice(d, "Hello World!");
    expect(d.getDevice().getString() == "") << "unflushed line buffer empty";
    printToDevice(d, " Yay!\n");
    expect(d.getDevice().getString() == "Hello World! Yay!\n") << "flushed line buffer not empty";
  };

  "floating points"_suite = [] {

    "float"_test = []
    {
      using matt::operator""_f;  // floats have to be wrapped to prevent promotion to double.

      // test cases to exercise all float branches at least once
      const std::tuple<const std::string, matt::Float> test_cases[] =
          {
              {"1.2345678E0", 1.2345678_f}, // 8
              {"1.234567E0", 1.234567_f}, // 7
              {"1.23456E0", 1.23456_f}, // 6
              {"1.2345E0", 1.2345_f}, // 5
              {"1.234E0", 1.234_f}, // 4
              {"1.23E0", 1.23_f}, // 3
              {"2.5E-1", 0.25_f}, // 2
              {"1.5E0", 1.5_f}, // 2
              {"5E-1", 0.5_f}, // 1 
              {"1E0", 1.0_f}, // 1
              {"NaN", matt::Float{std::numeric_limits<float>::quiet_NaN()}},
              {"Infinity", matt::Float{std::numeric_limits<float>::infinity()}},
              {"-Infinity", matt::Float{-std::numeric_limits<float>::infinity()}},
              {"0E0", 0.0_f},
              };


      for (auto &c : test_cases)
      {
        matt::StringDevice d;
        matt::printToDevice(d, std::get<1>(c));
        expect(d.getString() == std::get<0>(c)) << "failed " << std::get<0>(c);
      }
    };

    "double"_test = []
    {
      const std::tuple<const std::string, double> test_cases[] = 
          {
            {"1.2345678912345E-9", 1.2345678912345E-9},   // -ive and 1 digit exponent
            {"1.2345678912345E-99", 1.2345678912345E-99},   // -ive and 2 digit exponent
            {"1.2345678912345E199", 1.2345678912345E199}, // 3 digit exponent
            {"1.2345678912345E0", 1.2345678912345}, // 14
            {"1.234567891234E0", 1.234567891234}, // 13
            {"1.23456789123E0", 1.23456789123}, // 12
            {"1.2345678912E0", 1.2345678912}, // 11
            {"1.234567891E0", 1.234567891}, // 10
            {"1.23456789E0", 1.23456789}, // 9
            {"1.2345678E0", 1.2345678}, // 8
            {"1.234567E0", 1.234567}, // 7
            {"1.23456E0", 1.23456}, // 6
            {"1.2345E0", 1.2345}, // 5
            {"1.234E0", 1.234}, // 4
            {"1.23E0", 1.23}, // 3
            {"2.5E-1", 0.25}, // 2
            {"1.5E0", 1.5}, // 2
            {"5E-1", 0.5}, // 1 
            {"1E0", 1.0}, // 1
            {"NaN", std::numeric_limits<double>::quiet_NaN()},
            {"Infinity", std::numeric_limits<double>::infinity()},
            {"-Infinity", -std::numeric_limits<double>::infinity()},
            {"0E0", 0.0}
          };

      for (auto &c : test_cases)
      {
        matt::StringDevice d;
        matt::printToDevice(d, std::get<1>(c));
        expect(d.getString() == std::get<0>(c)) << "failed " << std::get<0>(c);
      }
    };

  }; // "floating points"_suite

//  "fails"_test = [] {
//    expect(that % false);
//  };

}
